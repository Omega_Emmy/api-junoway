var elasticsearch = require('elasticsearch');

var client = new elasticsearch.client({
	host: '172.31.27.118:9200',
	log: 'debug'
});

module.exports = client;
