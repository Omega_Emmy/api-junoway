var async = require('async');
var underscore = require('underscore');
var twitext = require('twitter-text');
var loopback = require('loopback');
var geocoder = require('./geocoder');

/**
 * MULTICASTING ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**/

exports.multicast = function (post, models, cb) {

	async.parallel([
		function (callback) {
			subscriber.findById(post.authorId, function(err,subscriber) {
				callback(err,subscriber.followers);
			});
		},
		function (callback, subscribers) { // subscriber's whose interest are in the post
			var tags = twitext.extractHashtags(post.content); // hashtag.parse(ctx.instance.content).tags;
			models.subscriber.find({where: {interests: {inq: tags}}}, function (err, interesteds){
				if (interesteds) 
					async.map(interesteds, function (interested, done) {
						done(null, interested.id);
					},function (err, interesteds) {
						subscriber = underscore.union(subscribers,interesteds);
						callback(err, subscribers);
					});
			});
		},
		function (callback, subscribers) { // subscribers mentioned in the post
			var mentions = twitext.extractMentions(post.content);
			models.subscriber.find({where: {username: mentions}}, function (err, mentions) {
				if (mentions)
					async.map(mentions, function (mention, done) {
						done(null,mention.id);
					},function (err, mentions) {
						subscribers = underscore(subscribers,mentions);
						callback(err,subscribers);
					});
			});
		}
	], function(err, subscribers) {
		// -send notification subscribers mentioned/following/interested
		if (!err)
			sendNotifications(subscribers,cb);
		else
			cb(err, null);
	});
};


/**
 * UNICASTING ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**/

exports.unicast = function (ctx, addressedTo, models, cb) {
	// TODO -send notification to addressed subscriber
};


/**
 * GEOCASTING +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**/

exports.geocast = function (post, models, cb) {
	models.subscriber.findOne({id:post.authorId}, function (err, author) {
		if (author) {
			var userConnect = subscriber.connect();
			var mode = userConnect.mode;
			switch(mode) {
				case 'location':
					models.subscriber.find({where: {and:[{gps: {near: userConnect.gps, maxDistance: userConnect.coverage, unit: userConnect.unit}}, {connect: {mode: 'location'}}]}},function(err,subscribers) {
						async.map(subscribers, function (subscriber, done) {
							if (receiver.gsp.distanceTo(author.gps) =< receiver.gsp) {
								done(null, subscriber.id);
							}
						}, function (err, subscribers) {
							sendNotifications(post,subscribers,models,cb);
						});
					});
				case 'city':
					geocoder.reverse({lat: userConnect.gps.lat, lon: userConnect.gps.lng}, function (err, data) {
						if (data.length > 0) {
							models.subscriber({include: 'connect'},function (err,subscribers) {
								async.filter(subscribers, function(subscriber, cb) {
									if(subscriber.connect.mode == 'city' || subscriber.connect.city == data[0].city)
										cb(null, true);
									else
										cb(null, false);
								}, function(err, subscribers) {
									if (!err && subscribers.length > 0) {
										getInterested(subscribers, post.content, function(err,subscribers) {
											async.map(subscribers,function(subscriber, done) {
												done(null, subscriber.id);
											},function(err, subscribers){
												sendNotifications(post,subscribers,models,cb);
											});
										})
									}
									else
										cb(err, null);	
								});
								
							});
						}
					});
				case 'country':
					geocoder.reverse({lat: userConnect.gps.lat, lon: userConnect.gps.lng}, function (err, data) {
						if (data.length > 0) {
							models.subscriber({include: 'connect'},function (err,subscribers) {
								async.filter(subscribers, function(subscriber, cb) {
									if(subscriber.connect.mode == 'country' || subscriber.connect.country == data[0].country)
										cb(null, true);
									else
										cb(null, false);
								}, function(err, subscribers) {
									if (!err && subscribers.length > 0) {
										getInterested(subscribers, post.content, function(err,subscribers) {
											async.map(subscribers,function(subscriber, done) {
												done(null, subscriber.id);
											},function(err, subscribers){
												sendNotifications(post,subscribers,models,cb);
											});
										})
									}
									else
										cb(err, null);	
								});
								
							});
						}
					});
				default:

			}
		}
	});
};

var getInterested = function (subscribers, content, callback) {
	var tags = twitext.extractHashtags(content);
	async.filter(subscribers, function (subscriber, done){
		var intersects = underscore(subscriber.interests, tags);
		if (intersects.length > 0)
			done(null, true);
		else
			done(null, false);
	}, function(err, subscribers){
		callback(err, subscribers);
	});
};

var sendNotifications = function (post,subscribers,models,callback) {
	async.parallel([
		function (cb) {
			// -Find author details
			models.subscriber.findOne({where: {id: post.author}}, cb);
		},
		function (cb, author) {
			if (author) 
				async.each(subscribers, function (subscriber, done) {
					models.notify.notifyByQuery({subscriberId: subscriber}, new models.notification({
						badge: 1,
						sound: 'ping.aiff',
						status: 'pending',
						created: Date(),
						messageFrom: subscriber.username,
						alert: 'New Broadcast'
					},done));
				}, callback);
		}
	]);
};
