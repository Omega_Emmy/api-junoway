'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var context = require('loopback-context');

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};
// -- Add your pre-processing middleware here --
app.use(context.perRequest());
app.use(loopback.token());
app.use(function setCurrentUser(req, res, next) {
  // console.log(req);
  if (!req.accessToken) {
    return next();
  }
  app.models.subscriber.findById(req.accessToken.userId, function(err, subscriber) {
    if (err) {
      return next(err);
    }
    if (!subscriber) {
      return next(new Error('No user with this access token was found.'));
    }
    var loopbackContext = context.getCurrentContext();
    if (loopbackContext) {
      loopbackContext.set('currentUser', subscriber);
      // loopbackContext.set('reqBody', req.body);
    }
    next();
  });
});

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
