'use strict';
var rp = require('request-promise');

module.exports = function(Offer) {
	var host = /*'api.junoway.com:9201';//*/'172.31.27.118:9201';

	// Overwrite find
	Offer.on('attached', function (app) {

		const origFind = Offer.find.bind(Offer);
		const origFindById = Offer.findById.bind(Offer);

		Offer.find = function (filter, done) {
			if (filter)
				filter = underscore.extend(filter, {include: 'author'});
			else
				filter = {include: 'author'};
			origFind(filter,done);
		};

		Offer.findById = function (id, filter, done) {
			if (filter)
				filter = underscore.extend(filter, {where: {id: id}, include: 'author'});
			else
				filter = {where: {id: id}, include: 'author'};
			origFindById(filter, done);
		};
	});

	Offer.observe('after save', function (ctx, next) {
		var instance = ctx.instance;
		if (ctx.isNewInstance)
			// get author's name
			Offer.app.models.subscriber.findOne({id: instance.authorId}, function (err, subscriber) {
				var username = "";
				if (subscriber)
					username = subscriber.username;
				// index the offer
				rp({
					method: 'POST',
					uri: 'http://'+host+'/junoway_v1_1/offer',
					json: true,
					body: {
						authorId: subscriber.id,
						author: username,
						product_service: instance.product_service,
						condition: instance.condition,
						description: instance.description,
						amount: instance.amount,
						rate_qty: instance.rate_qty,
						createdAt: instance.createdAt,
						updatedAt: instance.updatedAt
					}
				})
				.catch(function (err) {
					console.log(err);
				});
			});

		next();

	});

	Offer.search = function (query, done) {
		var uri = 'http://'+host+'/junoway_v1_1/offer/_search?q='+query;
		// console.log(uri);

		rp({
			method: 'GET',
			uri: uri,
			json: true
		})
		.then(function(res) {
			done(null, res.hits.hits);
		})
		.catch(function(err) {
			done(err);
		})
	};

	Offer.remoteMethod(
		'search',{
			accepts: {arg: 'query', type: 'string', http: {source: 'query'}, description: 'search for'},
			description: 'Search for a specific offer',
			returns: {type: 'array', root: true, description: 'array of matched products'},
      		http: {path: '/search', verb: 'get'}
		});

	Offer.advanced_search = function (query, done) {
		// console.log(query);
		var q = {
			"query": {
				"bool": {
					"must": [
						{
							"match": {"product_service": ""+query.pdt_desc}
						},
						{
							"match": {"condition": ""+query.cond }
						},
						{
							"match": {"author": ""+query.subscriber }
						}
					],
					"filter": {
						"range": {
							"amount": {
								"gte": 0,
								"lte": query.budget
							}
						}
					}
				}
			}
		};


		rp({
			method: 'POST',
			uri: 'http://'+host+'/junoway_v1_1/offer/_search',
			json: true,
			body: q
		})
		.then(function(resp) {
			done(null,resp.hits.hits);
		})
		.catch(function(err) {
			done(err);
		})
	}

	Offer.remoteMethod(
		'advanced_search',
		{
			description: "search with specifics",
			accepts:{arg: 'query', type: 'object', root: true, description: 'product name', http: {source: 'body'},default: {pdt_desc: 'string', cond: 'new', subscriber: 'string', budget: 0}},
			http: {path: '/advance_search', verb: 'post'},
			returns: {type: 'array', root: true}
		}
	);

	Offer.disableRemoteMethod('__updateById__attachments', false);
	Offer.disableRemoteMethod('__delete__attachments', false);
	Offer.disableRemoteMethod('__destroyById__attachments', false);

	Offer.disableRemoteMethod('__delete__bids', false);
	Offer.disableRemoteMethod('__destroyById__bids', false);
};
