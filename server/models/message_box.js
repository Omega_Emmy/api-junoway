'use strict';
var context = require('loopback-context');

module.exports = function(MessageBox) {

	MessageBox.beforeRemote('prototype.__create__messages', function(ctx, instance, next) {
		ctx.req.body.createdAt = Date.now();
		ctx.req.body.updatedAt = ctx.req.body.createdAt;
		next();
	});

	MessageBox.afterRemote('prototype.__create__messages', function(ctx, instance, next) {
		var authorId = instance.authorId;
		MessageBox.findOne({where: {subscriberId: instance.recipientId}}, function (err, messageBox){
			// console.log(messageBox);
			var message = {
				id: instance.id,
				authorId: instance.authorId,
				recipientId: instance.recipientId,
				body: instance.body,
				createdAt: instance.createdAt,
				updatedAt: instance.updatedAt
			};
			messageBox.messages.create(message, function(err) {
				if (err)
					console.log(err);
			});
			
		});
		next();
	});


	MessageBox.disableRemoteMethod('__delete__messages', false);
	MessageBox.disableRemoteMethod('__updateById__conversations', false);
	// MessageBox.disableRemoteMethod('__destroyById__messages', false);

};
