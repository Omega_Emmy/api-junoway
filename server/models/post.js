'use strict';
var underscore = require('underscore');

module.exports = function(Post) {

	// Overwrite find
	Post.on('attached', function (app) {

		const origPostFind = Post.find.bind(Post);
		const origPostFindById = Post.findById.bind(Post);

		Post.find = function (filter, options, done) {
			if (filter)
				filter = underscore.extend(filter, {include: 'author'});
			else
				filter = {include: 'author'};
			origPostFind(filter, done);
		};

		Post.findById = function (id, filter, done) {
			if (filter)
				filter = underscore.extend(filter, {where: {id: id}, include: 'author'});
			else
				filter = {where: {id: id}, include: 'author'};
			origPostFindById(filter, done);
		};
	});

};
