'use strict';

module.exports = function(Forum) {

	Forum.disableRemoteMethod('__delete__posts', false);
	Forum.disableRemoteMethod('__updateById__posts', false);
	Forum.disableRemoteMethod('__destroyById__posts', false);

};
