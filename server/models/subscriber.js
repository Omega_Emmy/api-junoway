'use strict';
var underscore = require('underscore');
var context = require('loopback-context');
var geocoder = require('../geocoder');
var async = require('async');

module.exports = function(Subscriber) {

	// Overwrite find
	/*Subscriber.on('attached', function (app) {

		const origFind = Subscriber.find.bind(Subscriber);
		const origFindById = Subscriber.findById.bind(Subscriber);

		Post.find = function (filter, done) {
			if (filter)
				filter = underscore.extend(filter, {include: 'author'});
			else
				filter = {include: 'author'};
			origFind(filter,done);
		};

		Subscriber.findById = function (id, filter, done) {
			if (filter)
				filter = underscore.extend(filter, {where: {id: id}, fields: {followerIds: false}});
			else
				filter = {where: {id: id}, fields: {followerIds: false}};
			origFindById(filter, done);
		};
	});*/

	Subscriber.afterRemote('create', function (ctx, instance, next) {
		// Create connect instance for new subscriber
		var body = ctx.req.body;
		async.parallel([
			function(cb) {
				if (body.gps) {
					var coordinates = body.gps.split(" ");
					geocoder.reverse({lat: coordinates[0], lon: coordinates[1]}, function (err, data) {
						if (!err && data.length > 0)
							instance.connect.create({
							  	"mode": "network",
							  	"gps": {lat: coordinates[0], lng: coordinates[1]},
							  	"country": data[0].country,
							  	"city": data[0].city,
							  	"coverage": 1,
							  	"type": "kilometers"
							},function (err,connect) {
								if (err)
									cb(err);
								else if (connect) // Create new city forum for new subscriber's city if it does not exist
									Subscriber.app.models.forum.findOrCreate({where: {city: connect.city}},{city: data[0].city}, cb);
							});
					});
				}
			},function(cb) {
				instance.message_box.create(cb);
				// subscriber.app.models.message_box.create({subscriber: instance.username}, cb);
			}
		], function (err, results) {
			if (err)
				console.log(err);
		});
		
		next();
	});

	

	Subscriber.disableRemoteMethod('__create__accessTokens', false);
	Subscriber.disableRemoteMethod('__delete__accessTokens', false);
	Subscriber.disableRemoteMethod('__updateById__accessTokens', false);
	Subscriber.disableRemoteMethod('__destroyById__accessTokens', false);

	Subscriber.disableRemoteMethod('__delete__broadcasts', false);
	Subscriber.disableRemoteMethod('__destroyById__broadcasts', false);

	Subscriber.disableRemoteMethod('__create__connect', false);
	Subscriber.disableRemoteMethod('__destroy__connect', false);

	Subscriber.disableRemoteMethod('__delete__followers', false);
	Subscriber.disableRemoteMethod('__create__followers', false);
	Subscriber.disableRemoteMethod('__updateById__followers', false);
	Subscriber.disableRemoteMethod('__destroyById__followers', false);
	Subscriber.disableRemoteMethod('__exists__followers', false);

	Subscriber.disableRemoteMethod('__delete__groups', false);
	Subscriber.disableRemoteMethod('__destroyById__groups', false);	
	Subscriber.disableRemoteMethod('__exists__groups', false);

	Subscriber.disableRemoteMethod('__create__message_box', false);
	Subscriber.disableRemoteMethod('__update__message_box', false);
	Subscriber.disableRemoteMethod('__destroy__message_box', false);
	Subscriber.disableRemoteMethod('__delete__offers', false);

	Subscriber.disableRemoteMethod('__create__posts', false);
	Subscriber.disableRemoteMethod('__delete__posts', false);
	Subscriber.disableRemoteMethod('__findById__posts', false);
	Subscriber.disableRemoteMethod('__updateById__posts', false);
	Subscriber.disableRemoteMethod('__destroyById__posts', false);


};
