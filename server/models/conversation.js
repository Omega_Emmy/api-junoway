'use strict';
var async = require('async');

module.exports = function(Conversation) {

	Conversation.on('attached', function (app) {
		const origFind = Conversation.find.bind(Conversation);
		const origFindById = Conversation.findById.bind(Conversation);

		Conversation.findById = function (id, done) {
			async.waterfall([
					function (cb) {
						Conversation.app.models.subscriber.findOne({where: {id: id}}, cb);
					},
					function (correspondent, cb) {
						Conversation.findOrCreate({where: {correspondentId: id}}, {correspondentId: id, correspondent: correspondent.username}, cb);						
					}
				], function (err, conversation) {
					done(err, conversation);
				});
		};
	});

	/*Conversation.beforeRemote('prototype.__create__messages', function (ctx, instance, next) {
		// console.log(instance);
		var body = ctx.req["body"];
		
		
	});*/

	Conversation.afterRemote('prototype.__create__messages', function (ctx, instance, next) {
		var body = instance;
		// check if the conversation exists
		async.parallel(
			[
				function(_cb) {
					Conversation.findById(body.recipientId, function (err, conversation) {
						if (conversation)
							conversation.updateAttribute('messageBoxId', body.authorId, _cb);
					});			
				},
				function(_cb) {
					Conversation.findById(body.authorId, function (err, conversation) {
						if (conversation)
							conversation.updateAttribute('messageBoxId', body.recipientId, _cb);
					});
				}
			],function (err, conversations) {
				conversations[1].messages.create(instance);
			}
		);
		next();
	});

	Conversation.disableRemoteMethod('__updateById__messages', false);
};
