'use strict';
var context = require('loopback-context');
var async = require('async');
var twitext = require('twitter-text');
var geocoder = require('../geocoder');
var underscore = require('underscore');
var rp = require('request-promise');

module.exports = function(Broadcast) {
	var host = /*'api.junoway.com:9201';//*/'172.31.27.118:9201';

	// Overwrite find
	Broadcast.on('attached', function (app) {

		const origFind = Broadcast.find.bind(Broadcast);
		const origFindById = Broadcast.findById.bind(Broadcast);
		Broadcast.findById = function (id, filter, done){
			if (filter)
				filter = underscore.extend(filter, {where: {id: id},include: 'author'});
			else
				filter = {where: {id: id},include: 'author'};
			origFind(filter, done);
		};
		Broadcast.find = function (filter, done) {
			var ctx = context.getCurrentContext();
			var currentUser = ctx && ctx.get('currentUser');
      // console.log(currentUser);
			if (filter)
				filter = underscore.extend(filter, {include: 'author'});
			else
				filter = {include: 'author'};

      // app.models.subscriber.findById(currentUser.id, function (err, sub) {
      //   console.log(sub);
      // });
			app.models.subscriber.findById(currentUser.id, function (err, subscriber) {
        console.log(subscriber);
				if(err)
					return done(err);
				subscriber.connect(function (err, connect) {
					console.log(connect);
					if (err)
						console.log(err);
					else
						switch(connect.mode.toLowerCase()) {

							case 'location':
								filter = underscore.extend(filter, {origin: {near: connect.gps, maxDistance: connect.coverage, type: connect.type}});
								origFind(filter, done);
								break;
							case 'city':
								origFind(filter,function(err, broadcasts) {
									if (err)
										return done(err);
									async.filter(broadcasts, function (broadcast, cb) {
										geocoder.reverse({lat: broadcast.origin.lat, lon: broadcast.origin.lng}, function (err,data) {
											if (err || data.length > 0)
												cb(null, false);
											else if(connect.city == data[0].city)
												var mentioned = underscore.intersection(twitext.extractMentions(broadcast.content),[subscriber.username]).length > 0 ? true : false;
												var interested = underscore.intersection(twitext.extractHashtags(broadcast.content), subscriber.interests).length > 0 ? true : false;
												if (mentioned || interested)
													cb(null, true);
										});
									},done);
								});
								break;
							case 'country':
								origFind(filter,function(err, broadcasts) {
									if (err)
										return done(err);
									async.filter(broadcasts, function (broadcast, cb) {
										geocoder.reverse({lat: broadcast.origin.lat, lon: broadcast.origin.lng}, function (err,data) {
											if (err || data.length > 0)
												cb(null, false);
											else if(connect.country == data[0].country);
												var mentioned = underscore.intersection(twitext.extractMentions(broadcast.content),[subscriber.username]).length > 0 ? true : false;
												var interested = underscore.intersection(twitext.extractHashtags(broadcast.content), subscriber.interests).length > 0 ? true : false;
												if (mentioned || interested)
													cb(null, true);
										});
									},done);
								});
								break;
							default:
								// console.log("default");
								origFind(filter,function (err, broadcasts) {
									if (broadcasts)
										// filter out those whose author is not being followed
										async.parallel([
											function (callback) {
												async.filter(broadcasts, function (broadcast, cb) {
													app.models.subscriber.findOne({id: broadcast.authorId}, function (err, author) {
														if (author && underscore.intersection(author.followers,[subscriber]).length > 0)
															cb(null, true);
														else
															cb(null, false);
													});
												},function (err, mentioneds) {
													callback(err, mentioneds);
												});
											},
											function (callback, mentions) {
												async.filter(broadcasts, function (broadcast, cb) {
													if (underscore.union(twitext.extractMentions(broadcast.content, [subscriber.username])))
														cb(null, true);
													else
														cb(null, false);
												}, function (err,followeds) {
													callback(err,followeds)
												});
											}
											], function (err, broadcasts) {
												done(err, underscore.union(broadcasts[0], broadcasts[1]));
											});
									else
										done(err,[]);
								});
						};
					// end else
				});// end connect_find

			});
		}

	});

	Broadcast.beforeRemote('create', function(ctx, instance, next) {
		// console.log(ctx.args);
		Broadcast.app.models.connect.findOne({where:{subscriberId: instance.authorId}}, function (err, connect) {
			if (connect) {
				ctx.args.data.origin = connect.gps;
				next();
			}
			else
				next();
		});
	});

	Broadcast.observe('after save', function (ctx, next) {
		var instance = ctx.instance;
		if (ctx.isNewInstance) // If new instance
			Broadcast.app.models.subscriber.findOne({id: instance.authorId
			}, function (err, subscriber) { // Get author username
				rp({
					method: 'POST',
					uri: 'http://'+host+'/junoway_v1-1/broadcast',
					json: true,
					body: {
						authorId: subscriber.id,
						author: subscriber.username,
						origin: {
							lat: instance.origin.lat,
							lon: instance.origin.lng
						},
						content: instance.content,
						createAt: instance.createAt,
						updateAt: instance.updateAt
					}
				})
				.catch(function (err) {
					console.log(err);
				})
			});

		next()
	});

	// Disable files CR(UD)
	Broadcast.disableRemoteMethod('__updateById__files', false);
	Broadcast.disableRemoteMethod('__delete__files', false);
	Broadcast.disableRemoteMethod('__destroyById__files', false);

	Broadcast.disableRemoteMethod('__updateById__attachments', false);
	Broadcast.disableRemoteMethod('__delete__attachments', false);
	Broadcast.disableRemoteMethod('__destroyById__attachments', false);

};
