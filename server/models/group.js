'use strict';
var context = require('loopback-context');

module.exports = function(Group) {

	Group.afterRemote('create', function(ctx, instance, next) {
		var contx = context.getCurrentContext();
		var currentUser = contx && contx.get('currentUser');
		instance.administrators.add(currentUser,function (err, admin) {
			if (err)
				console.log(err);
		});
		instance.members.add(currentUser, function (err, admin) {
			if (err)
				console.log(err);
		})

		next();
	});

	Group.disableRemoteMethod('__create__administrators', false);
	Group.disableRemoteMethod('__delete__administrators', false);
	Group.disableRemoteMethod('__updateById__administrators', false);
	Group.disableRemoteMethod('__destroyById__administrators', false);

	Group.disableRemoteMethod('__create__members', false);
	Group.disableRemoteMethod('__delete__members', false);
	Group.disableRemoteMethod('__updateById__members', false);
	Group.disableRemoteMethod('__destroyById__members', false);


	Group.disableRemoteMethod('__delete__posts', false);
	Group.disableRemoteMethod('__updateById__posts', false);
	Group.disableRemoteMethod('__destroyById__posts', false);
};
