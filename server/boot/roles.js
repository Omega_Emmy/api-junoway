module.exports = function (app) {
	var Role = app.models.Role;

	Role.registerResolver('groupAdmin', function (role, context, done) {

		function reject (err) {
	  	if (err) return done (err);
	      done(null, false);
	  }

	  if (context.modelName !== 'group')
      return reject(); // the target model is not a consultation

    var subscriberId = context.accessToken.subscriberId;
    
    if (!subscriberId)
      return reject(); // do not allow anonymous users

    // check if the user is an admin of the group
    if (context.modelId) {
    	app.models.group.findOne({where: {and: [{id: context.modelId},{adminIds: subscriberId}]}}, function (err, group) {
    		if (err || !group)
    			return reject();

    		done(null, true);
    	})
    }else {
      done(null,false);
    }
	});

	Role.registerResolver('groupMember', function (role, context, done) {
		
		function reject(err) {
			if (err) return done(err);
			done(null, false);
		}

		if (context.modelName !== 'group')
			return reject();

		var subscriberId = context.accessToken.subscriberId;

		if (!subscriberId)
			return reject();

		if(context.modelId) {
			app.models.membership.findOne({where: {and:[{groupId: context.modelId},{subscriberId: subscriberId}]}}, function (err, subscription) {
				if (err || !subscription)
					return reject();

				done(null, true);
			});
		}else {
			done(null, false);
		}
	});

}