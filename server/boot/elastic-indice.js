// var client = require('../elastic-client');
var rp = require('request-promise');
var host = /*'api.junoway.com:9201';//*/  '172.31.27.118:9201';

rp({
	method: 'HEAD',
	uri: 'http://'+host+'/junoway_v1_1',
	resolveWithFullResponse: true,
	simple: false
})
.then(function (res) {
	if (res.statusCode == 404)
		rp({
		method: 'PUT',
		uri: 'http://'+host+'/junoway_v1_1',
		body: {
			mappings: {
				broadcast: {
					properties: {
						authorId: {
							type: "string",
							index: "not_analyzed"
						},
						author: {
							type: "string",
							analyzer: "auto_complete"
						},
						origin: {
							type: "geo_point"
						},
						content: {
							type: "string",
							analyzer: "auto_complete"
						},
						createdAt: {
							type: "date"
						},
						updatedAt: {
							type: "date"
						}
					}
				},
				offer: {
					properties: {
						authorId: {
							type: "string",
							index: "not_analyzed"
						},
						author: {
							type: "string",
							analyzer: "auto_complete"
						},
						product_service: {
							type: "string",
							analyzer: "auto_complete"
						},
						condition: {
							type: "string",
							analyzer: "auto_complete"
						},
						description: {
							type: "string",
							analyzer: "auto_complete"
						},
						amount: {
							type: "double"
						},
						rate_qty: {
							type: "object"
						},
						createdAt: {
							type: "date"
						},
						updatedAt: {
							type: "date"
						}
					}
				}
			},
			settings: {
	      analysis: {
	        filter: {
	          auto_complete: {
	            type: "edge_ngram",
	            min_gram: 1,
	            max_gram: 50
	          }
	        },
	        analyzer: {
	          auto_complete: {
	            type: "custom",
	            tokenizer: "standard",
	            filter: [
	              "lowercase",
	              "auto_complete"
	            ]
	          }
	        }
	      }
	    }
		},
		json: true
	})
	.then(function (res) {
		console.log(res);
	})
	.catch(function (err) {
		console.log(err);
	})
})
.catch(function (err) {
	console.log(err);
});

