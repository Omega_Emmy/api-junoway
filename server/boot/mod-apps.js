var config = require('../config');

module.exports = function (app) {
	var Notification = app.models.notification;
  var Application = app.models.application;
  var PushModel = app.models.push;


  var junowayApp = {
    id: 'junoway-android-app',
    owner: 'junoway inc.',
    userId: 'strongloop',
    name: config.appName,

    description: 'LoopBack Push Notification Demo Application',
    pushSettings: {
      apns: {
        certData: config.apnsCertData,
        keyData: config.apnsKeyData,
        pushOptions: {
          // Extra options can go here for APN
        },
        feedbackOptions: {
          batchFeedback: true,
          interval: 300
        }
      },
      gcm: {
        serverApiKey: config.gcmServerApiKey
      }
    }
  };

  updateOrCreateApp(function (err, appModel) {
    if (err)
      console.log(err);
    if (appModel)
      console.log('Application id: %j', appModel.id);
  });

  //--- Helper functions ---
  function updateOrCreateApp(cb) {
    Application.findOne({
        where: { id: junowayApp.id }
      },
      function (err, result) {
        if (err) cb(err);
        if (result) {
          console.log('Updating application: ' - result.id);
          delete junowayApp.id;
          result.updateAttributes(junowayApp, cb);
        } else {
          return registerApp(cb);
        }
      });
  }

  function registerApp(cb) {
    console.log('Registering a new Application...');
    // Hack to set the app id to a fixed value so that we don't have to change
    // the client settings
    Application.beforeSave = function (next) {
      if (this.name === junowayApp.name) {
        this.id = 'junoway-android-app';
      }
      next();
    };
    Application.register(
      junowayApp.userId,
      junowayApp.name,
      {
        description: junowayApp.description,
        pushSettings: junowayApp.pushSettings
      },
      function (err, app) {
        if (err) {
          return cb(err);
        }
        return cb(null, app);
      }
    );
  }

}
